package me.justicepro.starutils;

import java.util.ArrayList;
import java.util.List;

public class Rank
{
	
	public String prefix = "";
	public String name = "";
	public List<String> permissions = new ArrayList<>();
	public String inheritPermission = null;
	
}