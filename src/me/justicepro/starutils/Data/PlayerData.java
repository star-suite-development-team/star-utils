package me.justicepro.starutils.Data;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import me.justicepro.starutils.StarFile;
import me.justicepro.starutils.StarType;

public class PlayerData extends StarFile
{
	
	public HashMap<String, Object> map = new HashMap<>();
	
	public PlayerData()
	{
		type = StarType.Player;
	}
	
	/**
	 * Save Player Data.
	 * @param data
	 * @param uuid
	 */
	public static void savePlayerData(PlayerData data, UUID uuid, File playerDir)
	{
		try
		{
			File file = new File(playerDir, uuid.toString() + ".star");
			Data.writeObjectToFile(file, data);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Read Player Data
	 * @param data
	 * @param uuid
	 */
	public static PlayerData readPlayerData(UUID uuid, File playerDir)
	{
		try
		{
			File file = new File(playerDir, uuid.toString() + ".star");
			if (!file.exists())
			{
				savePlayerData(new PlayerData(), uuid, playerDir);
				return new PlayerData();
			}
			return (PlayerData) Data.readObjectFromFile(file);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
}