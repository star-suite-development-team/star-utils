package me.justicepro.starutils.Data;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import me.justicepro.starutils.Rank;
import me.justicepro.starutils.StarFile;
import me.justicepro.starutils.StarType;

public class RankData extends StarFile
{
	
	public HashMap<String, Rank> ranks = new HashMap<>();
	
	public RankData()
	{
		type = StarType.Ranks;
	}
	
}