package me.justicepro.starutils.Data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.UUID;

public class Data
{
	
	/**
	 * Get an object from a file.
	 * @param file
	 * @return
	 * @throws IOException 
	 * @throws ClassNotFoundException
	 */
	public static Object readObjectFromFile(File file) throws IOException, ClassNotFoundException
	{
		ObjectInputStream input = new ObjectInputStream(new FileInputStream(file));
		Object object = input.readObject();
		input.close();
		return object;
	}
	
	/**
	 * Write an object to an file.
	 * @param file
	 * @param object
	 * @throws IOException
	 */
	public static void writeObjectToFile(File file, Serializable serializable) throws IOException
	{
		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(file));
		output.writeObject(serializable);
		output.flush();
		output.close();
	}
	
}